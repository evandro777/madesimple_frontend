import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

import router from './router'
import App from './App.vue'
import store from './store.js'

import 'bootstrap/dist/css/bootstrap.css'

Vue.use(VueAxios, axios)
Vue.use(store)

Vue.router = router
App.router = Vue.router

axios.defaults.baseURL = process.env.VUE_APP_BASEURL ? process.env.VUE_APP_BASEURL : 'http://localhost:8000/api'

axios.defaults.headers.common = { 'Accept': 'application/json' }
if (store.getters.token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${store.getters.token}`
}

axios.interceptors.response.use(function (response) {
        // Do something with response data
        return response;
    }, function (error) {
        // Redirect to login if token is expired
        if(error.response.status == 401 && error.response.data.message == 'Unauthenticated.'){
            store.dispatch('logout')
                .then(() => {
                    router.push('login')
                })
        }
        // Do something with response error
        return Promise.reject(error);
    })

new Vue(App).$mount('#app')