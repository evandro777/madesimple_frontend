import VueRouter from 'vue-router'
import Vue from 'vue'

import store from '@/store.js'
import Dashboard from '@/components/Dashboard.vue'
import Home from '@/components/Home.vue'
import Register from '@/components/Register.vue'
import Login from '@/components/Login.vue'

// Artists
import ArtistsCreate from '@/components/Artists/Create.vue';
import ArtistsEdit from '@/components/Artists/Edit.vue';
import ArtistsIndex from '@/components/Artists/Index.vue';

// Albums
import AlbumsCreate from '@/components/Albums/Create.vue';
import AlbumsEdit from '@/components/Albums/Edit.vue';
import AlbumsIndex from '@/components/Albums/Index.vue';

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'hash',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                auth: true
            }
        },

        // Artists
        {
            name: 'ArtistsCreate',
            path: '/artists/create',
            component: ArtistsCreate,
            meta: {
                auth: true
            }
        },
        {
            name: 'ArtistsEdit',
            path: '/artists/edit',
            component: ArtistsEdit,
            meta: {
                auth: true
            }
        },
        {
            name: 'ArtistsIndex',
            path: '/artists',
            component: ArtistsIndex,
            meta: {
                auth: true
            }
        },

        // Albums
        {
            name: 'AlbumsCreate',
            path: '/albums/create',
            component: AlbumsCreate,
            meta: {
                auth: true
            }
        },
        {
            name: 'AlbumsEdit',
            path: '/albums/edit',
            component: AlbumsEdit,
            meta: {
                auth: true
            }
        },
        {
            name: 'AlbumsIndex',
            path: '/albums',
            component: AlbumsIndex,
            meta: {
                auth: true
            }
        },
    ]
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.auth)) {
        if (!store.getters.token) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            let user = JSON.parse(localStorage.getItem('user'))
            if(to.matched.some(record => record.meta.is_admin)) {
                if(user.is_admin == 1){
                    next()
                }
                else{
                    next({ name: 'ArtistsIndex' })
                }
            }else {
                next()
            }
        }
    } else {
        next() 
    }
})

export default router